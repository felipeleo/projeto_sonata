<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Produto;

class HomeController extends Controller
{
  /**
  * @Route("/listar", name="listar_produto")
  */
  public function indexAction()
  {
    $produtos = $this->getDoctrine()
      ->getRepository('AppBundle:Produto')
      ->findAll();
    if(!$produtos){
      throw $this->createNotFoundException(
        'Nenhum produto foi cadastrado por enquanto.'
      );
    }
    return $this->render('default/list.html.twig', array('produtos' => $produtos));

  }

  /**
  * @Route("/criar", name="criar_produto")
  */
  public function createAction()
  {
    $produto = new Produto();
    $produto->setNome('Teclado');
    $produto->setPreco('35.00');
    $produto->setDescricao('Ergonomico e estiloso!');

    $em = $this->getDoctrine()->getManager();
    $em->persist($produto);
    $em->flush();
    return new Response('Novo produto salvo com o id: ' . $produto->getId());
  }

  /**
  *  @Route("/mostrar/{id}", name="mostrar_produto")
  */
  public function showAction($id)
  {
    $produto = $this->getDoctrine()
    ->getRepository('AppBundle:Produto')
    ->find($id);

    if(!$produto){
      throw $this->createNotFoundException(
      'Nenhum produto encontrado para o id: ' . $id
    );
  }
  return $this->render('default/show.html.twig', array('produto' => $produto));
  }

  /**
  * @Route("/produtos/{id}", name="cat_produtos")
  */
  public function showProdutosCat($id)
  {
    $categoria = $this->getDoctrine()
      ->getRepository('AppBundle:Categoria')
      ->find($id);

    $produtos = $categoria->getProdutos()->getValues();

      if(!$produtos){
        throw $this->createNotFoundException(
          'Nenhum produto encontrado para esta categoria'
        );
      }
      return $this->render('default/show.html.twig', array('produtos' => $produtos, 'categoria' => $categoria));
  }
}
