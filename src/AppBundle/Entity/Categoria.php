<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="categoria")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoriaRepository")
 */
class Categoria
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=255)
     */
    private $nome;

    /**
     * @ORM\OneToMany(targetEntity="Produto", mappedBy="categoria")
     */
     private $produtos;

     public function __construct()
     {
       $this->produtos = new ArrayCollection();
     }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     *
     * @return Categoria
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Add produto
     *
     * @param \AppBundle\Entity\Produto $produto
     *
     * @return Categoria
     */
    public function addProduto(\AppBundle\Entity\Produto $produto)
    {
        $this->produtos[] = $produto;

        return $this;
    }

    /**
     * Remove produto
     *
     * @param \AppBundle\Entity\Produto $produto
     */
    public function removeProduto(\AppBundle\Entity\Produto $produto)
    {
        $this->produtos->removeElement($produto);
    }

    /**
     * Get produtos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProdutos()
    {
        return $this->produtos;
    }
}
